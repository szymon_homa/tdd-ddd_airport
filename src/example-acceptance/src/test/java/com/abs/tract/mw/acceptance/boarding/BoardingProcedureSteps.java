/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.acceptance.boarding;

import com.abs.tract.mw.acceptance.CucumberWithSpringStepsBase;
import com.abs.tract.mw.application.BoardingStagesEventService;
import com.abs.tract.mw.model.flight.FlightNo;
import com.abs.tract.mw.model.flight.boarding.Boarding;
import com.abs.tract.mw.model.flight.boarding.BoardingRepository;
import com.abs.tract.mw.model.flight.boarding.BoardingStage;
import com.abs.tract.mw.model.flight.boarding.StageStatus;
import com.google.common.base.Optional;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import static org.junit.Assert.*;

/**
 *
 * @author shoma
 */
public class BoardingProcedureSteps extends CucumberWithSpringStepsBase {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private BoardingRepository boardingRepository;
	@Autowired
	private BoardingStagesEventService boardingEventService;

	@Before
	public void setUp() {
		mongoTemplate.getDb().dropDatabase();
	}

	@Given("^no passenger has yet started boarding procedure$")
	public void no_passenger_has_yet_started_boarding_procedure() throws Throwable {
	}

	@When("^first passenger of flight \"(.*?)\" dispatches a baggage$")
	public void first_passenger_of_flight_dispatches_a_baggage(String flightNo) throws Throwable {
		FlightNo flight = FlightNo.create(flightNo);
		boardingEventService.handleBoardingStageStart(flight, BoardingStage.BAGGAGE_DISPATCH);
	}

	@Then("^flight \"(.*?)\" has its boarding started$")
	public void flight_has_its_boarding_started(String flightNo) throws Throwable {
		Optional<Boarding> searchResult = findFlightsBoarding(flightNo);
		Boarding boarding = searchResult.get();
		assertTrue(boarding.hasBoardingStarted());
	}

	@Then("^baggage dispatch stage of flight \"(.*?)\" is marked as started$")
	public void baggage_dispatch_stage_of_flight_is_marked_as_started(String flightNo) throws Throwable {
		Optional<Boarding> searchResult = findFlightsBoarding(flightNo);
		Boarding boarding = searchResult.get();
		assertEquals(StageStatus.STARTED, boarding.getStatusOfStage(BoardingStage.BAGGAGE_DISPATCH));
	}

	@Given("^baggage dispatch, flight card disposal and passport check stages of a flight \"(.*?)\" are closed$")
	public void baggage_dispatch_flight_card_disposal_and_passport_check_stages_of_a_flight_are_closed(String flNo) throws Throwable {
		FlightNo flightNo = FlightNo.create(flNo);
		boardingEventService.handleBoardingStageStart(flightNo, BoardingStage.BAGGAGE_DISPATCH);
		boardingEventService.handleBoardingStageStart(flightNo, BoardingStage.FLIGHT_CARD_DISPOSAL);
		boardingEventService.handleBoardingStageStart(flightNo, BoardingStage.PASSPORT_CHECK);
		boardingEventService.handleBoardingStageStart(flightNo, BoardingStage.GATEWAY_CORSSING);

		boardingEventService.handleBoardingStageEnd(flightNo, BoardingStage.BAGGAGE_DISPATCH);
		boardingEventService.handleBoardingStageEnd(flightNo, BoardingStage.FLIGHT_CARD_DISPOSAL);
		boardingEventService.handleBoardingStageEnd(flightNo, BoardingStage.PASSPORT_CHECK);
	}

	@When("^last passenger of flight \"(.*?)\" is crossing the gateway$")
	public void last_passenger_of_flight_is_crossing_the_gateway(String flNo) throws Throwable {
		FlightNo flightNo = FlightNo.create(flNo);
		boardingEventService.handleBoardingStageEnd(flightNo, BoardingStage.GATEWAY_CORSSING);
	}

	@Then("^boarding of \"(.*?)\" should be marked as closed$")
	public void boarding_of_should_be_marked_as_closed(String flNo) throws Throwable {
		Optional<Boarding> searchResult = findFlightsBoarding(flNo);
		Boarding boarding = searchResult.get();
		assertTrue(boarding.hasBoardingFinished());
	}

	private Optional<Boarding> findFlightsBoarding(String flightNo) {
		Optional<Boarding> searchResult = boardingRepository.getBoardingFor(FlightNo.create(flightNo));
		return searchResult;
	}
}
