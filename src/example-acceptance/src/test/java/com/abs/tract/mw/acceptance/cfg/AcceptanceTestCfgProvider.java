/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.acceptance.cfg;

import com.abs.tract.mw.persistence.cfg.LocalEnvConfigurationProvider;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author shoma
 */
@Configuration
public class AcceptanceTestCfgProvider implements LocalEnvConfigurationProvider {

	@Bean
	@Override
	public Mongo mongo() {
		try {
			return new MongoClient();
		} catch (UnknownHostException ex) {
			throw new IllegalStateException(ex);
		}
	}

	@Override
	public String dbName() {
		return "AcceptanceTestDB";
	}
}
