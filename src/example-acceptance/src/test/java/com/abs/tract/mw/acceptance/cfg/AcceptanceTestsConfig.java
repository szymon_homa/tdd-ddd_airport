/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.acceptance.cfg;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author shoma
 */
@Configuration
@ComponentScan("com.abs.tract.mw.*")
public class AcceptanceTestsConfig {
}
