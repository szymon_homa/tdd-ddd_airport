/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.acceptance;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 *
 * @author shoma
 */
@RunWith(Cucumber.class)
@CucumberOptions(glue = {"com.abs.tract.mw", "cucumber.api.spring"})
public class RunAcceptanceTest {
}
