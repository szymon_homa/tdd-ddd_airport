/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.acceptance;

import com.abs.tract.mw.acceptance.cfg.AcceptanceTestsConfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

/**
 *
 * @author shoma
 */
@ContextConfiguration(classes = {AcceptanceTestsConfig.class})
@TestExecutionListeners(value = {DependencyInjectionTestExecutionListener.class})
public class CucumberWithSpringStepsBase {
}
