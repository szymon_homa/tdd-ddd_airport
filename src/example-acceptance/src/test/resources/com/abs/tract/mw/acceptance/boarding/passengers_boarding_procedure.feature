Feature: monitoring of whole flow of passengers boarding procedure.
	As an airport employee who is responsible for monitoring and conducting the passengers boarding. 
	I want to be able to monitor current boarding states of the planned flight, 
	be informed of every deviation from the boarding plan and be informed whenever such deviations were already solved by the system (and when not). 
	So it will give me a possibility to react on every unwanted situation that might occur, 
	without focusing too much, on issues caused by temporary communication problems etc.

	I'm only interested in monitoring such boarding stages: baggage dispatch, flight card disposal,
	passport check, gateway crossing.

	Scenario: First passenger dispatched a baggage
	
	Given no passenger has yet started boarding procedure
	When first passenger of flight "FLT01" dispatches a baggage
	Then flight "FLT01" has its boarding started
	And baggage dispatch stage of flight "FLT01" is marked as started 

	Scenario: All passengers has passed through baggage dispatch, flight card disposal and passport check stages
	and last passenger is crossing the gateway.
	
	Given baggage dispatch, flight card disposal and passport check stages of a flight "FLT01" are closed
	When last passenger of flight "FLT01" is crossing the gateway
	Then boarding of "FLT01" should be marked as closed

	Scenario: System reports that passport check stage has started before flight card disposal stage

	Given no passenger has yet started boarding procedure
	When system receives information that first passenger of flight "FLT01" has just went through passport check
	Then system should mark passport check stage as started
	And produce an Alert for flight "FLT01" with issue "Out of order boarding stages start, passport check started before flight card disposal"

	Scenario: System reports that flight card disposal has started before baggage dispatch stage

	Given no passenger has yet started boarding procedure
	When first passenger of flight "FLT01" has just received a flight card
	Then flight "FLT01" has its boarding started
	And system should mark passport check stage of flight "FLT01" as started

	Scenario: System reports that passport check stage has been finished before flight card disposal
	
	Given both flight card disposal and passport check stages are started for flight "FLT01"
	When system receives information that passport check stage of flight "FLT01" has finished
	Then system should mark passport check stage of flight "FLT01" as finished 
	And produce an Alert for flight "FLT01" with issue "Out of order boarding stages end, passport check finished before flight card disposal"

	Scenario: System reports that passport check stage has been finished before it was started
	
	Given flight card disposal stage of flight "FLT01" is opened
	And passport check stage of flight "FLT01" is closed
	When system receives information that passport check stage of flight "FLT01" has finished
	Then system should mark passport check stage of flight "FLT01" as finished 
	And produce an Alert for flight "FLT01" with issue "Stage passport check was finished before it was started"

	Scenario: System receives a lacking information of boarding stage start

	Given system reported passport check stage of flight "FLT01" start 
	When system reports flight card disposal stage of flight "FLT01" start
	Then flight card disposal stage of flight "FLT01" should be marked as started
	And Alert for flight "FLT01" with issue "Out of order boarding stages start, passport check started before flight card disposal" has been automatically resolved
	