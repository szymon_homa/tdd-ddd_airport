/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

import com.abs.tract.mw.model.flight.FlightNo;

/**
 *
 * @author shoma
 */
public class BoardingFixture {

	public static Boarding createBoarding() {
		return createBoarding(FlightNo.create("defaultNo"));
	}

	public static Boarding createBoarding(FlightNo no) {
		return new Boarding(no);
	}
}
