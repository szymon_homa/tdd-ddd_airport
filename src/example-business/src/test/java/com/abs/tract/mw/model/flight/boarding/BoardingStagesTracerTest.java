/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

import static com.abs.tract.mw.model.flight.boarding.BoardingFixture.createBoarding;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author shoma
 */
public class BoardingStagesTracerTest {

	public BoardingStagesTracerTest() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void stagesTracerStagesStatusAfterCreation() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();

		assertThatAllStagesAreAtState(stagesTracer, StageStatus.NOT_YET_STARTED);
	}

	@Test
	public void stagesTracerStagesStatusWhenBaggageDispatchHasStarged() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();

		stagesTracer.markAsStarted(BoardingStage.BAGGAGE_DISPATCH);

		StageStatus baggageDispatchStatus = stagesTracer.getStatusOfStage(BoardingStage.BAGGAGE_DISPATCH);
		assertEquals(StageStatus.STARTED, baggageDispatchStatus);
	}

	@Test
	public void hasBoardingStartedWhenNoStageHasBeenStarted() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();

		boolean hasBoardingStarted = stagesTracer.hasBoardingStarted();

		assertFalse(hasBoardingStarted);
	}

	@Test
	public void hasBoardingStartedWhenAtLeastOneStageHasBeenStarted() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();
		stagesTracer.markAsStarted(BoardingStage.BAGGAGE_DISPATCH);

		boolean hasBoardingStarted = stagesTracer.hasBoardingStarted();

		assertTrue(hasBoardingStarted);
	}

	@Test
	public void hasBoardingStartedWhenOneStageIsFinishedAndOthersNotYetStarted() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();
		stagesTracer.markAsStarted(BoardingStage.BAGGAGE_DISPATCH);
		stagesTracer.markAsFinished(BoardingStage.BAGGAGE_DISPATCH);

		boolean hasBoardingStarted = stagesTracer.hasBoardingStarted();

		assertTrue(hasBoardingStarted);
	}

	@Test
	public void hasBoardingFinishedWhenNoStageHasStartedYet() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();

		boolean hasBoardingFinished = stagesTracer.hasBoardingFinished();

		assertFalse(hasBoardingFinished);
	}

	@Test
	public void hasBoardingFinishedWhenSomeStagesFinished() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();

		stagesTracer.markAsFinished(BoardingStage.BAGGAGE_DISPATCH);
		boolean hasBoardingFinished = stagesTracer.hasBoardingFinished();

		assertFalse(hasBoardingFinished);
	}

	@Test
	public void hasBoardingFinishedWhenAllStagesFinished() {
		BoardingStagesTracer stagesTracer = createBoardingStagesTracer();

		stagesTracer.markAsFinished(BoardingStage.BAGGAGE_DISPATCH);
		stagesTracer.markAsFinished(BoardingStage.FLIGHT_CARD_DISPOSAL);
		stagesTracer.markAsFinished(BoardingStage.PASSPORT_CHECK);
		stagesTracer.markAsFinished(BoardingStage.GATEWAY_CORSSING);
		boolean hasBoardingFinished = stagesTracer.hasBoardingFinished();

		assertTrue(hasBoardingFinished);
	}

	private void assertThatAllStagesAreAtState(BoardingStagesTracer stagesTracer, StageStatus expected) {
		StageStatus baggageDispatchStatus = stagesTracer.getStatusOfStage(BoardingStage.BAGGAGE_DISPATCH);
		StageStatus flightCardDisposalStatus = stagesTracer.getStatusOfStage(BoardingStage.FLIGHT_CARD_DISPOSAL);
		StageStatus gatewayCrossingStatus = stagesTracer.getStatusOfStage(BoardingStage.GATEWAY_CORSSING);
		StageStatus passportCheckStatus = stagesTracer.getStatusOfStage(BoardingStage.PASSPORT_CHECK);
		assertEquals(expected, baggageDispatchStatus);
		assertEquals(expected, flightCardDisposalStatus);
		assertEquals(expected, gatewayCrossingStatus);
		assertEquals(expected, passportCheckStatus);
	}

	private BoardingStagesTracer createBoardingStagesTracer() {
		return new BoardingStagesTracer();
	}
}