/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

import com.abs.tract.mw.model.flight.FlightNo;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static com.abs.tract.mw.model.flight.boarding.BoardingFixture.*;

/**
 *
 * @author shoma
 */
public class BoardingTest {

	private final FlightNo firstNo = FlightNo.create("firstNo");
	private final FlightNo secondNo = FlightNo.create("secondNo");

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void hasSameIdentityWhenSameFlightNoPassed() {
		Boarding firstBoarding = createBoarding();
		Boarding secondBoarding = createBoarding();

		assertTrue(firstBoarding.hasSameIdentity(secondBoarding));
	}

	@Test
	public void hasSameIdentityWhenDifferentFlightNoPassed() {
		Boarding firstBoarding = createBoarding(firstNo);
		Boarding secondBoarding = createBoarding(secondNo);

		assertFalse(firstBoarding.hasSameIdentity(secondBoarding));
	}

	@Test
	public void acknowledgeChange() {
		StageChangeEvent event = mock(StageChangeEvent.class);
		Boarding boarding = createBoarding();

		boarding.acknowledgeChange(event);

		verify(event).driveChange(anyBoardingStagesTracer());
	}

	private BoardingStagesTracer anyBoardingStagesTracer() {
		return any(BoardingStagesTracer.class);
	}
}