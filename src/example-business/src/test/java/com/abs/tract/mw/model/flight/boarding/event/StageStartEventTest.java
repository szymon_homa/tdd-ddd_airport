/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding.event;

import com.abs.tract.mw.model.flight.boarding.BoardingStage;
import com.abs.tract.mw.model.flight.boarding.BoardingStagesTracer;
import com.abs.tract.mw.model.flight.boarding.StageStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author shoma
 */
public class StageStartEventTest {

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testDriveChangeOfPickedStage() {
		BoardingStage pickedStage = BoardingStage.PASSPORT_CHECK;
		BoardingStagesTracer tracer = new BoardingStagesTracer();
		StageStartEvent event = new StageStartEvent(pickedStage);

		event.driveChange(tracer);

		assertEquals(StageStatus.STARTED, tracer.getStatusOfStage(pickedStage));
	}
}