/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author shoma
 */
public class FlightNoTest {

	String sameValue;

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void createWithNonEmptyNonNullString() {
		FlightNo flightNo = FlightNo.create("nonEmptyNonNull");

		assertNotNull(flightNo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createWithEmptyString() {
		FlightNo.create("      ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void createWithNull() {
		FlightNo.create(null);
	}

	@Test
	public void isSameValueWhenSamePassed() {
		String sameValue = "sameValue";
		FlightNo firstFlightNo = FlightNo.create(sameValue);
		FlightNo secondFlightNo = FlightNo.create(sameValue);

		assertTrue(firstFlightNo.isSameValue(secondFlightNo));
	}

	@Test
	public void isSameValueWhenNotSamePassed() {
		FlightNo firstFlightNo = FlightNo.create("firstFlightNo");
		FlightNo secondFlightNo = FlightNo.create("secondFlightNo");

		assertFalse(firstFlightNo.isSameValue(secondFlightNo));
	}
}