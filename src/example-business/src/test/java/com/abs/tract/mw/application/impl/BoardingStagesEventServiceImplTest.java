/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.application.impl;

import com.abs.tract.mw.model.flight.FlightNo;
import com.abs.tract.mw.model.flight.boarding.Boarding;
import com.abs.tract.mw.model.flight.boarding.BoardingRepository;
import com.abs.tract.mw.model.flight.boarding.BoardingStage;
import com.abs.tract.mw.model.flight.boarding.StageStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.*;
import static com.abs.tract.mw.model.flight.boarding.BoardingFixture.*;
import com.abs.tract.mw.model.flight.boarding.event.StageStartEvent;
import com.google.common.base.Optional;
import org.junit.Ignore;

/**
 *
 * @author shoma
 */
public class BoardingStagesEventServiceImplTest {

	private final BoardingRepository repository = mock(BoardingRepository.class);
	private final BoardingStagesEventServiceImpl eventService = new BoardingStagesEventServiceImpl(repository);
	private final FlightNo flightWithAssoiciatedBoarding = FlightNo.create("flightWithAssoiciatedBoarding");
	private final FlightNo flightWithNoBoardingAssociated = FlightNo.create("flightWithNoBoardingAssociated");

	@Before
	public void setUp() {
		when(repository.getBoardingFor(anyFlightNo())).thenReturn(Optional.<Boarding>absent());
	}

	@After
	public void tearDown() {
	}

	@Test
	public void whenStartEventComesForNotExistingBoarding() {

		eventService.handleBoardingStageStart(flightWithNoBoardingAssociated, BoardingStage.BAGGAGE_DISPATCH);

		ArgumentCaptor<Boarding> boardingCaptor = ArgumentCaptor.forClass(Boarding.class);
		verify(repository).store(boardingCaptor.capture());
		Boarding storedBoarding = boardingCaptor.getValue();
		asserBoardingAssociatedWithFlightNo(flightWithNoBoardingAssociated, storedBoarding);
		assertBoardingStageStatus(storedBoarding, BoardingStage.BAGGAGE_DISPATCH, StageStatus.STARTED);
	}

	@Test
	public void whenStartEventComesForExistingBoarding() {
		Boarding existingBoarding = createBoarding(flightWithAssoiciatedBoarding);
		when(repository.getBoardingFor(flightWithAssoiciatedBoarding)).thenReturn(Optional.of(existingBoarding));

		eventService.handleBoardingStageStart(flightWithAssoiciatedBoarding, BoardingStage.BAGGAGE_DISPATCH);

		verify(repository).store(existingBoarding);
		assertBoardingStageStatus(existingBoarding, BoardingStage.BAGGAGE_DISPATCH, StageStatus.STARTED);
	}

	@Test
	public void whenEndEventComesForAllreadyStartedBoardingStage() {
		Boarding boarding = createBoarding(flightWithAssoiciatedBoarding);
		BoardingStage stage = BoardingStage.BAGGAGE_DISPATCH;
		when(repository.getBoardingFor(flightWithAssoiciatedBoarding)).thenReturn(Optional.of(boarding));
		boarding.acknowledgeChange(new StageStartEvent(stage));

		eventService.handleBoardingStageEnd(flightWithAssoiciatedBoarding, stage);

		verify(repository).store(boarding);
		assertBoardingStageStatus(boarding, stage, StageStatus.FINISHED);
	}

	@Ignore(value = "I don't know how this should behave - time to check with")
	@Test
	public void whenEndEventComesForNonExistingBoardingStage() {
		BoardingStage stage = BoardingStage.BAGGAGE_DISPATCH;

		eventService.handleBoardingStageEnd(flightWithNoBoardingAssociated, stage);

	}

	void asserBoardingAssociatedWithFlightNo(FlightNo flightWithNoBoardingAssociated, Boarding storedBoarding) {
		assertEquals(flightWithNoBoardingAssociated, storedBoarding.getFlightNo());
	}

	void assertBoardingStageStatus(Boarding storedBoarding, BoardingStage stage, StageStatus expectedStatus) {
		assertEquals(expectedStatus, storedBoarding.getStatusOfStage(stage));
	}

	FlightNo anyFlightNo() {
		return any(FlightNo.class);
	}
}