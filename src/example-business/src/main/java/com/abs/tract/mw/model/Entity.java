/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model;

/**
 *
 * @author shoma
 */
public interface Entity<T extends Entity<? extends Entity>> {

	boolean hasSameIdentity(T entity);
}
