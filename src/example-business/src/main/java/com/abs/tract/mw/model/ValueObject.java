/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model;

import java.io.Serializable;

/**
 *
 * @author shoma
 */
public interface ValueObject<T extends ValueObject> extends Serializable {

	public boolean isSameValue(T valueObject);
}
