/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

import com.abs.tract.mw.model.Entity;
import com.abs.tract.mw.model.flight.FlightNo;
import java.util.Objects;

/**
 *
 * @author shoma
 */
public class Boarding implements Entity<Boarding> {

	private final FlightNo id;
	private final BoardingStagesTracer stagesTracer = new BoardingStagesTracer();

	public Boarding(FlightNo id) {
		this.id = id;
	}

	@Override
	public boolean hasSameIdentity(Boarding entity) {
		return id.equals(entity.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && getClass().equals(obj.getClass())) {
			Boarding boarding = (Boarding) obj;
			return boarding.hasSameIdentity(this);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.id);
		return hash;
	}

	public void acknowledgeChange(StageChangeEvent event) {
		event.driveChange(stagesTracer);
	}

	public FlightNo getFlightNo() {
		return id;
	}

	public boolean hasBoardingStarted() {
		return stagesTracer.hasBoardingStarted();
	}

	public boolean hasBoardingFinished() {
		return stagesTracer.hasBoardingFinished();
	}

	public StageStatus getStatusOfStage(BoardingStage boardingStage) {
		return stagesTracer.getStatusOfStage(boardingStage);
	}
}
