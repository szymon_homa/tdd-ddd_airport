/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

import com.abs.tract.mw.model.ValueObject;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author shoma
 */
public class BoardingStagesTracer implements ValueObject<BoardingStagesTracer> {

	private final Map<BoardingStage, StageStatus> stagesWithStatus = new EnumMap<>(BoardingStage.class);

	public BoardingStagesTracer() {
		resetState();
	}

	@Override
	public boolean isSameValue(BoardingStagesTracer valueObject) {
		return valueObject.stagesWithStatus.equals(stagesWithStatus);
	}

	public boolean hasBoardingStarted() {
		return stagesWithStatus.values().contains(StageStatus.STARTED)
			|| stagesWithStatus.values().contains(StageStatus.FINISHED);
	}

	public boolean hasBoardingFinished() {
		Set<StageStatus> existingStatuses = fetchUniqueExistingStatuses();
		return existingStatuses.contains(StageStatus.FINISHED)
			&& existingStatuses.size() == 1;
	}

	private Set<StageStatus> fetchUniqueExistingStatuses() {
		Collection<StageStatus> allStatuses = stagesWithStatus.values();
		return new HashSet<>(allStatuses);
	}

	public void markAsStarted(BoardingStage boardingStage) {
		stagesWithStatus.put(boardingStage, StageStatus.STARTED);
	}

	public void markAsFinished(BoardingStage boardingStage) {
		stagesWithStatus.put(boardingStage, StageStatus.FINISHED);
	}

	public StageStatus getStatusOfStage(BoardingStage boardingStage) {
		return stagesWithStatus.get(boardingStage);
	}

	private void resetState() {
		for (BoardingStage stage : BoardingStage.values()) {
			stagesWithStatus.put(stage, StageStatus.NOT_YET_STARTED);
		}
	}
}
