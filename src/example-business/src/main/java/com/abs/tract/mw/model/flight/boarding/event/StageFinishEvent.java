/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding.event;

import com.abs.tract.mw.model.flight.boarding.BoardingStage;
import com.abs.tract.mw.model.flight.boarding.BoardingStagesTracer;
import com.abs.tract.mw.model.flight.boarding.StageChangeEvent;

/**
 *
 * @author shoma
 */
public class StageFinishEvent implements StageChangeEvent {

	private final BoardingStage stageToFinish;

	public StageFinishEvent(BoardingStage stageToFinish) {
		this.stageToFinish = stageToFinish;
	}

	@Override
	public void driveChange(BoardingStagesTracer tracer) {
		tracer.markAsFinished(stageToFinish);
	}
}
