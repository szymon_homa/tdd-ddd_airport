/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.application.impl;

import com.abs.tract.mw.application.BoardingStagesEventService;
import com.abs.tract.mw.model.flight.FlightNo;
import com.abs.tract.mw.model.flight.boarding.Boarding;
import com.abs.tract.mw.model.flight.boarding.BoardingRepository;
import com.abs.tract.mw.model.flight.boarding.BoardingStage;
import com.abs.tract.mw.model.flight.boarding.event.StageFinishEvent;
import com.abs.tract.mw.model.flight.boarding.event.StageStartEvent;
import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shoma
 */
@Service
public class BoardingStagesEventServiceImpl implements BoardingStagesEventService {

	private final BoardingRepository repository;

	@Autowired
	public BoardingStagesEventServiceImpl(BoardingRepository repository) {
		this.repository = repository;
	}

	@Override
	public void handleBoardingStageStart(FlightNo flightNo, BoardingStage stage) {
		Boarding boarding = getExistingOrCreateNewBoarding(flightNo);
		boarding.acknowledgeChange(new StageStartEvent(stage));
		repository.store(boarding);
	}

	@Override
	public void handleBoardingStageEnd(FlightNo flightNo, BoardingStage stage) {
		Optional<Boarding> searchResult = repository.getBoardingFor(flightNo);
		Boarding boarding = searchResult.get();
		boarding.acknowledgeChange(new StageFinishEvent(stage));
		repository.store(boarding);
	}

	private Boarding getExistingOrCreateNewBoarding(FlightNo flightNo) {
		Optional<Boarding> boardingSearchResult = repository.getBoardingFor(flightNo);
		if (boardingSearchResult.isPresent()) {
			return boardingSearchResult.get();
		} else {
			return new Boarding(flightNo);
		}
	}
}
