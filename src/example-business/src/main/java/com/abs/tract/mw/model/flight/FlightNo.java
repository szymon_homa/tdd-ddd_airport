/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight;

import com.abs.tract.mw.model.ValueObject;
import java.util.Objects;

/**
 *
 * @author shoma
 */
public class FlightNo implements ValueObject<FlightNo> {

	public static FlightNo create(String flightNo) {
		throwExceptionWhenNullPassed(flightNo);
		throwExceptionWhenEmptyPassed(flightNo);
		return new FlightNo(flightNo);
	}

	private static void throwExceptionWhenNullPassed(String flightNo) throws IllegalArgumentException {
		if (flightNo == null) {
			throw new IllegalArgumentException("FlightNo can not be created for null string");
		}
	}

	private static void throwExceptionWhenEmptyPassed(String flightNo) throws IllegalArgumentException {
		if (flightNo.trim().isEmpty()) {
			throw new IllegalArgumentException("FlightNo can not be created for an empty string");
		}
	}
	private final String flightNo;

	private FlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	@Override
	public boolean isSameValue(FlightNo valueObject) {
		return flightNo.equals(valueObject.flightNo);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && getClass().equals(obj.getClass())) {
			FlightNo fltNo = (FlightNo) obj;
			return isSameValue(fltNo);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 47 * hash + Objects.hashCode(this.flightNo);
		return hash;
	}
}
