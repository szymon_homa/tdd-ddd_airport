/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.application;

import com.abs.tract.mw.model.flight.FlightNo;
import com.abs.tract.mw.model.flight.boarding.BoardingStage;
import com.abs.tract.mw.model.flight.boarding.StageStatus;

/**
 *
 * @author shoma
 */
public interface BoardingStagesEventService {

	public void handleBoardingStageStart(FlightNo flightNo, BoardingStage stage);

	public void handleBoardingStageEnd(FlightNo flightNo, BoardingStage stage);
}
