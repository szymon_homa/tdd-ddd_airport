/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

/**
 *
 * @author shoma
 */
public enum StageStatus {

	NOT_YET_STARTED,
	STARTED,
	FINISHED
}
