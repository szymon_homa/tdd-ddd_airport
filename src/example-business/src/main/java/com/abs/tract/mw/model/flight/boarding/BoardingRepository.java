/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

import com.abs.tract.mw.model.flight.FlightNo;
import com.google.common.base.Optional;

/**
 *
 * @author shoma
 */
public interface BoardingRepository {

	Optional<Boarding> getBoardingFor(FlightNo flightNo);

	void store(Boarding boarding);
}
