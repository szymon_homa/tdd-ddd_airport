/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

/**
 *
 * @author shoma
 */
public interface StageChangeEvent {

	public void driveChange(BoardingStagesTracer tracer);
}
