/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.model.flight.boarding;

/**
 *
 * @author shoma
 */
public enum BoardingStage {

	BAGGAGE_DISPATCH,
	FLIGHT_CARD_DISPOSAL,
	PASSPORT_CHECK,
	GATEWAY_CORSSING
}
