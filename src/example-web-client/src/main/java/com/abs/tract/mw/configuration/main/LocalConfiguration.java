/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.configuration.main;

import com.abs.tract.mw.persistence.cfg.LocalEnvConfigurationProvider;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoFactoryBean;

/**
 *
 * @author shoma
 */
@Configuration
public class LocalConfiguration implements LocalEnvConfigurationProvider {

	@Bean
	@Override
	public Mongo mongo() {
		try {
			return new MongoClient("localhost");
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	@Override
	public String dbName() {
		return "milkyway-production";
	}
}
