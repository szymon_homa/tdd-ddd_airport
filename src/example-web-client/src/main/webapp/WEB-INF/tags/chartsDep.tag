<%@tag description="all dependencies required by charts library" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<t:includeCSS styleSheetName="nv.d3.min"/>
<t:includeJS jsName="d3.v3.min" />
<t:includeJS jsName="nv.d3" />


