<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="pageTitle" %>
<%@attribute name="addHead" fragment="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${pageTitle}</title>
		<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet" type="text/css" />
		<link href="<c:url value="/resources/css/validation.css" />" rel="stylesheet" type="text/css" />
		<jsp:invoke fragment="addHead"/>
    </head>
    <body>
		<div class="root">
			<div class="main">
				<jsp:doBody/>
			</div>
		</div>
    </body>
</html>
