<%@tag description="css link" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="styleSheetName" required="true"%>
<link href="<c:url value="/resources/css/${styleSheetName}.css" />" rel="stylesheet" type="text/css" />
