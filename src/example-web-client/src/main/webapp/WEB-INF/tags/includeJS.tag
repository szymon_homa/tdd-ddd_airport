<%@tag description="js script" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="jsName" required="true"%>
<script type="text/javascript" src="<c:url value="/resources/js/${jsName}.js" />"></script>

