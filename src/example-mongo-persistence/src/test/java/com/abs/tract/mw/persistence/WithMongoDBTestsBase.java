/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.persistence;

import com.abs.tract.mw.persistence.cfg.PersistenceConfiguration;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

/**
 *
 * @author shoma
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners(value = {DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {PersistenceConfiguration.class})
public abstract class WithMongoDBTestsBase {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Before
	public final void setUp() {
		mongoTemplate.getDb().dropDatabase();
		testSetUp();
	}

	protected MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	protected abstract void testSetUp();
}
