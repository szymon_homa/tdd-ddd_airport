/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.persistence.boarding;

import com.abs.tract.mw.model.Entity;
import com.abs.tract.mw.model.flight.FlightNo;
import com.abs.tract.mw.model.flight.boarding.Boarding;
import com.abs.tract.mw.model.flight.boarding.BoardingStage;
import com.abs.tract.mw.model.flight.boarding.StageStatus;
import com.abs.tract.mw.model.flight.boarding.event.StageStartEvent;
import com.abs.tract.mw.persistence.WithMongoDBTestsBase;
import com.google.common.base.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author shoma
 */
public class MongoDbBoardingRepositoryTest extends WithMongoDBTestsBase {

	@Autowired
	private SpringDataMongoDBBoardingUtilRepository boardingUtilRepository;
	@Autowired
	private MongoDbBoardingRepository boardingRepository;
	private final FlightNo flightNo = FlightNo.create("a flight no");

	@Override
	protected void testSetUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testStoreNewBoarding() {
		Boarding toStore = createBoarding(flightNo);

		boardingRepository.store(toStore);

		Optional<Boarding> searchResult = boardingRepository.getBoardingFor(flightNo);
		assertSameIdentity(toStore, searchResult.get());
	}

	@Test
	public void testGetBoardingWhenNoBoardingPersisted() {
		Optional<Boarding> searchResult = boardingRepository.getBoardingFor(flightNo);

		assertFalse(searchResult.isPresent());
	}

	@Test
	public void testUpdateStoredBoarding() {
		Boarding boarding = createBoarding(flightNo);
		boardingRepository.store(boarding);

		boarding.acknowledgeChange(new StageStartEvent(BoardingStage.BAGGAGE_DISPATCH));
		boardingRepository.store(boarding);

		Optional<Boarding> searchResult = boardingRepository.getBoardingFor(flightNo);
		Boarding updatedBoarding = searchResult.get();
		assertSameIdentity(boarding, updatedBoarding);
		assertEquals(StageStatus.STARTED, boarding.getStatusOfStage(BoardingStage.BAGGAGE_DISPATCH));
		assertTotallPersistedObjectsCount(1);
	}

	private <T extends Entity> void assertSameIdentity(T expected, T asserted) {
		assertTrue(expected.hasSameIdentity(asserted));
	}

	private Boarding createBoarding(FlightNo flightNo) {
		return new Boarding(flightNo);
	}

	private void assertTotallPersistedObjectsCount(int expectedCount) {
		assertEquals(expectedCount, boardingUtilRepository.count());
	}
}