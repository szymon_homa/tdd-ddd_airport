/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.persistence.cfg;

import com.abs.tract.mw.persistence.AutoScanPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author shoma
 */
@Configuration
@ComponentScan(basePackageClasses = {AutoScanPoint.class,})
@EnableMongoRepositories(basePackageClasses = {AutoScanPoint.class})
public class PersistenceConfiguration {

	@Autowired
	private LocalEnvConfigurationProvider configurationProvider;

	@Bean
	public MongoDbFactory mongoDbFactory() {
		return new SimpleMongoDbFactory(configurationProvider.mongo(), configurationProvider.dbName());
	}

	@Bean
	public MongoTemplate mongoTemplate() {
		return new MongoTemplate(mongoDbFactory());
	}
}
