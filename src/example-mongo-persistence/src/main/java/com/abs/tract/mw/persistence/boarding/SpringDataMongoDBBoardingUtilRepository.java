/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.persistence.boarding;

import com.abs.tract.mw.model.flight.FlightNo;
import com.abs.tract.mw.model.flight.boarding.Boarding;
import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author shoma
 */
public interface SpringDataMongoDBBoardingUtilRepository extends MongoRepository<Boarding, FlightNo> {
}
