/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.persistence.cfg;

import com.mongodb.Mongo;
import org.springframework.data.mongodb.core.MongoFactoryBean;

/**
 *
 * @author shoma
 */
public interface LocalEnvConfigurationProvider {

	public Mongo mongo();

	public String dbName();
}
