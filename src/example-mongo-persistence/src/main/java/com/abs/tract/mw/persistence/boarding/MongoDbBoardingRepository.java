/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abs.tract.mw.persistence.boarding;

import com.abs.tract.mw.model.flight.FlightNo;
import com.abs.tract.mw.model.flight.boarding.Boarding;
import com.abs.tract.mw.model.flight.boarding.BoardingRepository;
import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shoma
 */
@Repository
public class MongoDbBoardingRepository implements BoardingRepository {

	private final SpringDataMongoDBBoardingUtilRepository mongoDbRepository;

	@Autowired
	public MongoDbBoardingRepository(SpringDataMongoDBBoardingUtilRepository mongoDbRepository) {
		this.mongoDbRepository = mongoDbRepository;
	}

	@Override
	public Optional<Boarding> getBoardingFor(FlightNo flightNo) {
		Boarding foundOne = mongoDbRepository.findOne(flightNo);
		return Optional.fromNullable(foundOne);
	}

	@Override
	public void store(Boarding boarding) {
		mongoDbRepository.save(boarding);
	}
}
