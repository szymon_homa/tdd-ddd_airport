# Proposed stories of an Airport maintenance application
User Stories:

1. ** Monitoring of whole flow of passengers boarding procedure:** 
As an airport employee who is responsible for monitoring and conducting the passengers boarding. 
I want to be able to monitor current boarding states of the planned flight, 
be informed of every deviation from the boarding plan and be informed whenever such deviations were already solved by the system (and when not). 
So it will give me a possibility to react on every unwanted situation that might occur, 
without focusing too much, on issues caused by temporary communication problems

==============================================================================

2. **Monitorowanie dostępności pasów startowych:**
Jako pracownik wieży kontroli lotów,
chcę mieć możliwość monitorowania każdego pasa startowego,
tak bym mógł bez wypadków kierować ruchem przylotów i odlotów.

3. **Zarządzanie kolejnością startów i lądowań:**
Jako pracownik wieży kontroli lotów, 
chcę mieć możliwość przypisywania konkretnych pasów statowych do konkretnych lotów,
zezwalając im na start lub lądowanie,
co pozwoli mi sprawnie sterować ruchem, nie powodując opóźnień.

4. **Automatyczne wybieranie pierwszego wolnego pasa lądowania:**
Jako pracownik wieży kontroli lotów,
chcę by system automatycznie podpowiadał mi który pas jest akurat wolny,
lub który z pasów najszybciej będzie nadawał się do ponownego startu/lądowania,
dzięki czemu system przyspieszy moją pracę tam gdzie decyzja nie wymaga niestandardowego działania.